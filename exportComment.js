

function ConvertToCSV(objArray) {
  var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
  var str = '';

  for (var i = 0; i < array.length; i++) {
    var line = '';
    for (var index in array[i]) {
      if (line != '') line += ','

      array[i][index] = array[i][index].replace('"', " ");
      line += '"' + array[i][index] + '"';
    }
    line = line.replace(/(\r\n|\n|\r)/gm, "");

    str += line + '\r\n';
  }

  return str;
}

function download_str(str, filename) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(str));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
}

var comments = document.querySelectorAll("ytcp-comment-thread");
var export_array = [];
export_array[0] = ["Comment", "Name", "Time", "Video"];
var i = 1;
comments.forEach(comment => {
  export_array[i] = {
    "comment": comment.querySelector("ytcp-comment-expander").innerText,
    "name": comment.querySelector("#metadata a").innerText,
    "time": comment.querySelector(".published-time-text").innerText,
    "video": comment.querySelector("ytcp-comment-video-thumbnail #video-title").innerText,
  };
  i++;
});
var csvstr = ConvertToCSV(export_array);
download_str(csvstr, "comment.csv");